package cases;

import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.core.io.ClassPathResource;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.AlertsPage;
import pages.LoginPage;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.fail;


public class AlertsCase {

    private final String LOGIN_PAGE_URL = "http://way2automation.com/way2auto_jquery/index.php";
    private final String ALERTS_PAGE_URL = "http://way2automation.com/way2auto_jquery/alert.php";
    private String userName = "";
    private String userPassword = "";
    private String alertMessage = "";
    private WebDriver driver;
    private AlertsPage alertsPage;
    private LoginPage loginPage;

    @BeforeClass
    public void setUp() throws Exception {
        FirefoxDriverManager.getInstance().version("0.19.1").setup();
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        initProperties();
    }

    @Test
    public void testAlerts() throws IOException {
        logIn();
        openAlertWindow();
        setAlertContent();
        Assert.assertTrue(alertsPage.getAlertContent().getText().contains(alertMessage));
    }

    private void setAlertContent() {
        alertsPage.clickElement(alertsPage.getAlertDemoEl());
        Alert alert = driver.switchTo().alert();
        alert.sendKeys(alertMessage);
        alert.accept();
    }


    private void openAlertWindow() {
        driver.get(ALERTS_PAGE_URL);
        alertsPage = new AlertsPage(driver);
        alertsPage.clickElement(alertsPage.getMainMenu());
        driver.switchTo().frame(alertsPage.getFrameInputAlert());
    }

    private void logIn() {
        driver.get(LOGIN_PAGE_URL);
        loginPage = new LoginPage(driver);
        loginPage.signInBtnClick();
        loginPage.enterUserName(userName);
        loginPage.enterPassword(userPassword);
        loginPage.submitClick();
    }

    private void initProperties() throws IOException {
        Properties config = new Properties();
        try {
            config.load(new ClassPathResource("config.properties").getInputStream());
            userName = config.getProperty("way2.user.userName");
            userPassword = config.getProperty("way2.user.password");
            alertMessage = config.getProperty("way2.alert.message");
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("File 'config.properties' can't be open!");
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.quit();
    }
}
