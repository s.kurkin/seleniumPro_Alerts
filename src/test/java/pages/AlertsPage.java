package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class AlertsPage extends BasePage {

    //Main menu "Input Alert"
    @FindBy(xpath = "//li/a[contains(text(),'Input Alert')]") ////li[@class='active']/a
    private WebElement mainMenuEl;

    //Alert demo button
    @FindBy(xpath = "//button[contains(text(),'')]")
    private WebElement alertDemoBtn;

    //Frame
    @FindBy(xpath = ".//div[@id='example-1-tab-2']//iframe")
    private WebElement frameInputAlertEl;

    @FindBy(xpath = ".//*[@id='demo']")
    private WebElement alertContent;

    public AlertsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getMainMenu() {
        return getElement(mainMenuEl);
    }

    public WebElement getFrameInputAlert() {
        return getElement(frameInputAlertEl);
    }

    public WebElement getAlertDemoEl() {
        return getElement(alertDemoBtn);
    }

    public WebElement getAlertContent() {
        return getElement(alertContent);
    }
}
