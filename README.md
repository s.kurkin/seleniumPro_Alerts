Задание:
1) открыть http://way2automation.com/way2auto_jquery/alert.php
2) Нажать Input Alert
3) Нажать кнопку, ввести кастомный текст, подтвердить
4) Убедиться, что текст применился
     
Запуск теста:
1) В файле \src\test\resources\config.properties заполнить проперти соответствующими значениями:  
way2.user.userName - логин пользователя
way2.user.password - пароль пользователя
way2.alert.message - обращение к пользователю
	 

1) mvn clean -Dtest=AlertsCase test